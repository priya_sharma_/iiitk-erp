var slots = [ 8, 9, 10, 11, 12, 13, 14, 15, 16 ];
var days = [ "monday", "tuesday", "wednesday", "thursday", "friday" ];

(function() {
    
      var current;
	var initEventHandlers = function() {
		$(".draggable").draggable({
			helper : "clone"
		});
		$(".droppable").droppable({
			accept : function(element) {
				
			
				if (element.hasClass('subject-label'))
					return $(this).find('.subject-label').length === 0;
				
				if (element.hasClass('faculty-label')){
					
					return $(this).find('.faculty-label').length === 0;
					
				}
					if (element.hasClass('venue-label'))
					return $(this).find('.venue-label').length === 0;
				
					
			},
			 drop: function( event, ui ) {
                var el;
                var xmlhttp;
                alert(ui.draggable.context.innerHTML);
                alert(event.target.id);
				try{
					xmlhttp = new XMLHttpRequest();
				} catch (e){
					// Internet Explorer Browsers
					try{
						xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
					} catch (e) {
						try{
							xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
						} catch (e){
						//Browser doesn't support ajax	
							alert("Your browser is unsupported");
						}
					}
				}	
				var data;
				if(xmlhttp){
					
				    xmlhttp.onreadystatechange=function() {
				    	
				        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				        	data=xmlhttp.responseText;
				        	
				        	alert(current.find('.faculty-label').length);
						}
				        if(xmlhttp.status == 404)
							alert("Could not connect to server");
						}
				    
				    xmlhttp.open("POST","FacultyClash",false);    
					xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
				    xmlhttp.send("day="+"monday"+"&time="+"8"+"&faculty="+"TAPAN KUMAR JAIN");
				    
				   
				}
				if(data!="false") return false; //change to true
				
                if(ui.draggable.hasClass('dropped'))
                    el = ui.draggable;
                else
                    el = ui.draggable.clone().addClass('dropped');
                el.find('.remove').remove();
             
				
                el.append('<span class="remove">&times;</span>');
                $(this).append(el);
                $(el).draggable({
                    helper: "clone"
                });
            }
        });
        $('#timetable').on('click','.remove', function() {
            $(this).parent().remove();
        });
    };
	initEventHandlers();
})();


function save() {
    var department = window.location.href.substring(window.location.href.indexOf("=")+1, window.location.href.indexOf("&"));
    var semester = window.location.href.substring(window.location.href.indexOf("&")+5, window.location.href.lastIndexOf("&"));
	var batch = window.location.href.substring(window.location.href.lastIndexOf("=")+1);
    
	var json_array = [];
	
	for ( var i = 0; i < 5; i++) {
		for ( var j = 0; j < 9; j++) {

			var json_object = {};

			if (document
					.getElementById("time_slot_" + slots[j] + "_" + days[i])
					.getElementsByClassName(
							"label label-default draggable subject-label").length != 0) {

				var subject_name = document.getElementById(
						"time_slot_" + slots[j] + "_" + days[i])
						.getElementsByClassName(
								"label label-default draggable subject-label")[0].innerHTML;
				subject_name = subject_name.substring(0, subject_name
						.indexOf('<'));
				json_object['subject_name'] = subject_name;
			}

			if (document
					.getElementById("time_slot_" + slots[j] + "_" + days[i])
					.getElementsByClassName(
							"label label-default draggable faculty-label").length != 0) {

				var faculty_name = document.getElementById(
						"time_slot_" + slots[j] + "_" + days[i])
						.getElementsByClassName(
								"label label-default draggable faculty-label")[0].innerHTML;
				faculty_name = faculty_name.substring(0, faculty_name
						.indexOf('<'));
				json_object['faculty_name'] = faculty_name;
			}

			if (document
					.getElementById("time_slot_" + slots[j] + "_" + days[i])
					.getElementsByClassName(
							"label label-default draggable venue-label").length != 0) {

				var venue_name = document.getElementById(
						"time_slot_" + slots[j] + "_" + days[i])
						.getElementsByClassName(
								"label label-default draggable venue-label")[0].innerHTML;
				venue_name = venue_name.substring(0, venue_name.indexOf('<'));
				json_object['venue_name'] = venue_name;

			}
			if ((document.getElementById(
					"time_slot_" + slots[j] + "_" + days[i])
					.getElementsByClassName(
							"label label-default draggable subject-label").length != 0)
					|| (document
							.getElementById(
									"time_slot_" + slots[j] + "_" + days[i])
							.getElementsByClassName(
									"label label-default draggable faculty-label").length != 0)
					|| (document
							.getElementById(
									"time_slot_" + slots[j] + "_" + days[i])
							.getElementsByClassName(
									"label label-default draggable venue-label").length != 0)) {

				json_object["day"] = days[i];
				json_object["time_slot"] = slots[j];
				json_object["batch"]=batch;
				json_object["sem"]=semester;
				json_object["dept"]=department;
			}

			// if(json_object)
			if (json_object.hasOwnProperty('subject_name') || json_object.hasOwnProperty('faculty_name') || json_object.hasOwnProperty('venue_name')) {
				json_array.push(json_object);
			}
	
//		    alert(JSON.stringify(json_array));
			window.json_ar=JSON.stringify(json_array);
//			
		}

	}
	var xmlhttp;
	try{
		xmlhttp = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
			//Browser doesn't support ajax	
				alert("Your browser is unsupported");
			}
		}
	}	
	
	if(xmlhttp){
	    xmlhttp.onreadystatechange=function() {
	    	
	        if (xmlhttp.readyState==4 && xmlhttp.status==200) {
				
			}
	        if(xmlhttp.status == 404)
				alert("Could not connect to server");
			}
	    ;
	    xmlhttp.open("POST","SaveTimeTable",true);
		xmlhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
	    xmlhttp.send("json_list="+json_ar);
	   
	}
}



	function pass(){
		var sDept = document.getElementById("depts");
		var dept = sDept.options[sDept.selectedIndex].value;
		
		var sbatch = document.getElementById("batchs");
		var batch = sbatch.options[sbatch.selectedIndex].value;
		
		var sSem = document.getElementById("sems");
		var sem = sSem.options[sSem.selectedIndex].value;
		window.location.assign('http://localhost:8080/erp/timetableGenerate.jsp?dept='+dept+'&batch='+batch+'&sem='+sem);
	}
	

	
	

	
	
