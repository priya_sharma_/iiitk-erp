<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<%@page import="timeTableGeneration.timetableConnect"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Timetable Generation</title>
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="vendor/jquery-ui/jquery-ui.min.css" />
    <link rel="stylesheet" href="vendor/jquery-ui/jquery-ui.theme.min.css" />
    <link rel="stylesheet" href="dist/css/style.css" />
</head>
<body>

<div class="container">
    <div class="row">
        <div class="col-xs-6">
            <h1 class="text-center">Timetable Generation</h1>
        </div>
        
        <div class="col-xs-12 faculty">
        <%
        ArrayList<String> faculty_list=timetableConnect.getTimetableFaculty(
        		Integer.parseInt(request.getParameter("dept")),
        			Integer.parseInt(request.getParameter("sem")));
        	Iterator<String> faculty_iterator =	faculty_list.iterator();
        	while(faculty_iterator.hasNext()){
        		
        	String current=faculty_iterator.next();
        		
        %>
            <span class="label label-default draggable faculty-label" data-faculty="<%=current %>" ><%=current %></span>
        <%} %>
        </div>
        
        <div class="col-xs-12 subject">
        <%
        ArrayList<String> subject_list=timetableConnect.getTimetableSubjects(
        		Integer.parseInt(request.getParameter("dept")),
        			Integer.parseInt(request.getParameter("sem")));
        			
        	Iterator<String> subject_iterator=	subject_list.iterator();
        	while(subject_iterator.hasNext()){
        		
        	String current=subject_iterator.next();
        		
        %>
        <span class="label label-default draggable subject-label" data-subject="<%=current %>" ><%=current %></span> 
        <%} %>
       
           
        </div>
        
        
        <div class="col-xs-12 venue">
           <%
        ArrayList<String> venue_list = timetableConnect.getTimetableVenue();
       	Iterator<String> venue_iterator = venue_list.iterator();
        	while(venue_iterator.hasNext()){
        		
        	String current=venue_iterator.next();
        		
        %>
            <span class="label label-default draggable venue-label" data-venue="<%=current %>" ><%=current %></span>
        <%} %>
         
         </div>
        
        <div class="col-xs-12">
            <table class="table table-bordered" id="timetable">
                
                <thead>
                <tr>
                	<th></th>
                    <th>08:00</th>
                    <th>09:00</th>
                    <th>10:00</th>
                    <th>11:00</th>
                    <th>12:00</th>
                    <th>13:00</th>
                    <th>14:00</th>
                    <th>15:00</th>
                    <th>16:00</th>
                </tr>
                
                </thead>
                <tbody id="table_body">
                <tr id="time_slot_monday">
                    <td>Monday</td>
                    <td class="droppable" id="time_slot_8_monday"></td>
                    <td class="droppable" id="time_slot_9_monday"></td>
                    <td class="droppable" id="time_slot_10_monday"></td>
                    <td class="droppable" id="time_slot_11_monday"></td>
                    <td class="droppable" id="time_slot_12_monday"></td>
                    <td class="droppable" id="time_slot_13_monday"></td>
                    <td class="droppable" id="time_slot_14_monday"></td>
                    <td class="droppable" id="time_slot_15_monday"></td>
                    <td class="droppable" id="time_slot_16_monday"></td>
                    
                </tr>
                
                 <tr id="time_slot_tuesday">
                    <td>Tuesday</td>
                    <td class="droppable" id="time_slot_8_tuesday"></td>
                    <td class="droppable" id="time_slot_9_tuesday"></td>
                    <td class="droppable" id="time_slot_10_tuesday"></td>
                    <td class="droppable" id="time_slot_11_tuesday"></td>
                    <td class="droppable" id="time_slot_12_tuesday"></td>
                    <td class="droppable" id="time_slot_13_tuesday"></td>
                    <td class="droppable" id="time_slot_14_tuesday"></td>
                    <td class="droppable" id="time_slot_15_tuesday"></td>
                    <td class="droppable" id="time_slot_16_tuesday"></td>
                    
                </tr>
               
                 <tr id="time_slot_wednesday">
                    <td>Wednesday</td>
                    <td class="droppable" id="time_slot_8_wednesday"></td>
                    <td class="droppable" id="time_slot_9_wednesday"></td>
                    <td class="droppable" id="time_slot_10_wednesday"></td>
                    <td class="droppable" id="time_slot_11_wednesday"></td>
                    <td class="droppable" id="time_slot_12_wednesday"></td>
                    <td class="droppable" id="time_slot_13_wednesday"></td>
                    <td class="droppable" id="time_slot_14_wednesday"></td>
                    <td class="droppable" id="time_slot_15_wednesday"></td>
                    <td class="droppable" id="time_slot_16_wednesday"></td>
                    
                </tr>
                
                <tr id="time_slot_thursday">
                    <td>Thursday</td>
                    <td class="droppable" id="time_slot_8_thursday"></td>
                    <td class="droppable" id="time_slot_9_thursday"></td>
                    <td class="droppable" id="time_slot_10_thursday"></td>
                    <td class="droppable" id="time_slot_11_thursday"></td>
                    <td class="droppable" id="time_slot_12_thursday"></td>
                    <td class="droppable" id="time_slot_13_thursday"></td>
                    <td class="droppable" id="time_slot_14_thursday"></td>
                    <td class="droppable" id="time_slot_15_thursday"></td>
                    <td class="droppable" id="time_slot_16_thursday"></td>
                    
                </tr>
                
                <tr id="time_slot_friday">
                    <td>Friday</td>
                    <td class="droppable" id="time_slot_8_friday"></td>
                    <td class="droppable" id="time_slot_9_friday"></td>
                    <td class="droppable" id="time_slot_10_friday"></td>
                    <td class="droppable" id="time_slot_11_friday"></td>
                    <td class="droppable" id="time_slot_12_friday"></td>
                    <td class="droppable" id="time_slot_13_friday"></td>
                    <td class="droppable" id="time_slot_14_friday"></td>
                    <td class="droppable" id="time_slot_15_friday"></td>
                    <td class="droppable" id="time_slot_16_friday"></td>
                    
                </tr>
                
                  </tbody>
            </table>
        </div>
        <button class="btn btn-success col-xs-3" onclick="save()">Save</button>
       
    </div>
</div>
<input type="hidden" id="save" name="saveTT"/>


<script src="vendor/jquery/jquery.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="vendor/handlebars.js"></script>
<script src="dist/js/application.js"></script>
</body>
</html>