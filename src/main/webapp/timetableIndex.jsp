<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Timetable Generation</title>
    <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css" />
    <link rel="stylesheet" href="vendor/jquery-ui/jquery-ui.min.css" />
    <link rel="stylesheet" href="vendor/jquery-ui/jquery-ui.theme.min.css" />
    <link rel="stylesheet" href="dist/css/style.css" />
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-xs-6">
            <h1 class="text-center">IIIT Kota Timetable Generation</h1>
        </div>
    </div>
    <div class="row">
    	<div class="col-xs-12">
    		<form action="timetableGenerate.jsp" method="GET">
    			<div   class="form-group col-xs-3 text-center">
    				<select id="depts" name="dept" class="form-control col-xs-8">
    					<option>Department</option>
    					<option value="1">CSE</option>
                        <option value="2">ECE</option>
    				</select>
    			</div>
    			<div class="form-group col-xs-3 text-center">
    				<select id="sems" name="sem" class="form-control">
    					<option>Semester</option>
    					<option value="1">1st Semester</option>
                        <option value="2">2nd Semester</option>
                        <option value="3">3rd Semester</option>
                        <option value="4">4th Semester</option>
                        <option value="5">5th Semester</option>
                        <option value="6">6th Semester</option>
                        <option value="7">7th Semester</option>
                        <option value="8">8th Semester</option>                        
    				</select>
    			</div>
    			<div class="form-group col-xs-3 text-center">
    				<select id="batchs" name="batch" class="form-control">
    					<option>Batch</option>
    					<option value="1">1st Batch</option>
                        <option value="2">2nd Batch</option>
                        <option value="3">3rd Batch</option>
                        <option value="4">4th Batch</option>
    				</select>
    			</div>
    			<button class="btn btn-success col-xs-3"  type="submit">Go</button>
    		</form>
    	</div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-6">
            <h1 class="text-center">Faculty Timetable</h1>
        </div>
    </div>
    <div class="row">
    	<div class="col-xs-12">
    		<form action="facultyTimetable.jsp" method="POST">
    			<div class="form-group col-xs-3 text-center">
    				<select name="fac" class="form-control col-xs-8">
    					<option>Faculty</option>
    					<option value="1">Tapan Kumar Jain</option>
                        <option value="2">Pooja Jain</option>
    				</select>
    			</div>
    			
    			<button class="btn btn-success col-xs-3"  type="submit">Go</button>
    		</form>
    	</div>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-6">
            <h1 class="text-center">Venue-wise Timetable</h1>
        </div>
    </div>
    <div class="row">
    	<div class="col-xs-12">
    		<form action="venueTimetable.jsp" method="POST">
    			<div class="form-group col-xs-3 text-center">
    				<select name="venue_timetable" class="form-control col-xs-8">
    					<option>Venue</option>
    					<option value="1">TR-7</option>
                        <option value="2">TR-6</option>
    				</select>
    			</div>
    			 
       			<button class="btn btn-success col-xs-3"  type="submit" onsubmit="pass();">Go</button>
    		</form>
    	</div>
    </div>
</div>

<script src="vendor/jquery/jquery.js"></script>
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="vendor/jquery-ui/jquery-ui.min.js"></script>
<script src="vendor/handlebars.js"></script>
<script src="dist/js/application.js"></script>
</body>
</html>