/**
 * 
 */
package timeTableGeneration;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;

import org.json.JSONArray;

import users.Student;
import exceptions.IncorrectFormatException;

/**
 * @author Priya
 *
 */
public class timetableConnect {

static Connection conn ;
	
	public static Connection getConnection() throws SQLException{

		if(conn==null){
			try {
				Class.forName("org.postgresql.Driver");
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			conn = DriverManager
					.getConnection("jdbc:postgresql://172.16.1.231:5432/iiitk",
							"developer", "developer");
		}
		return conn;
	}

	public static ArrayList<String> getTimetableSubjects(int dept_id, int sem) throws SQLException{
		
		PreparedStatement proc = getConnection().prepareStatement("SELECT public.\"getTimetableInitialDataSubjects\"(?,?);");
		proc.setInt(1,dept_id);
		proc.setInt(2,sem);
		
		ResultSet rs=proc.executeQuery();
		rs.next();
		System.out.println(proc);
		ArrayList<String> Subjects=new ArrayList<String>();
		JSONArray j_array=new JSONArray(rs.getString(1));
		for(int i=0;i<j_array.length();i++){
			j_array.getJSONObject(i).get("sub_name").toString();
			Subjects.add(j_array.getJSONObject(i).get("sub_name").toString());
		}
		rs.close();
		proc.close();
		return Subjects;
	}
	
public static ArrayList<String> getTimetableFaculty(int dept_id, int sem) throws SQLException{
		
		PreparedStatement proc = getConnection().prepareStatement("SELECT public.\"getTimetableInitialDataFaculties\"(?,?);");
		proc.setInt(1,dept_id);
		proc.setInt(2,sem);
		
		ResultSet rs=proc.executeQuery();
		rs.next();
		System.out.println(proc);
		ArrayList<String> Faculty=new ArrayList<String>();
		JSONArray j_array=new JSONArray(rs.getString(1));
		for(int i=0;i<j_array.length();i++){
			j_array.getJSONObject(i).get("fac_name").toString();
			Faculty.add(j_array.getJSONObject(i).get("fac_name").toString());
		}
		rs.close();
		proc.close();
		return Faculty;
	}

public static ArrayList<String> getTimetableVenue() throws SQLException{
	
	PreparedStatement proc = getConnection().prepareStatement("SELECT public.\"getTimetableInitialDataVenue\"();");
	System.out.println(proc);
	ResultSet rs=proc.executeQuery();
	rs.next();
	
	ArrayList<String> Venue=new ArrayList<String>();
	JSONArray j_array=new JSONArray(rs.getString(1));
	for(int i=0;i<j_array.length();i++){
		j_array.getJSONObject(i).get("venue_name").toString();
		Venue.add(j_array.getJSONObject(i).get("venue_name").toString());
	}
	rs.close();
	proc.close();
	return Venue;
}
  


	public static void main(String[] args) {
		try {
			getTimetableSubjects(1,2);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}


